import { createRouter, createWebHashHistory } from 'vue-router'

import ListView from '../views/ListView.vue'

const routes = [
  {
    path: '/listView',
    name: 'list',
    component: ListView
  },
  {
    path: '/gridList',
    name: 'grid-List',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/gridList.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
